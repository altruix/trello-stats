package cc.altruix.trellostats;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.fest.assertions.api.Assertions;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Created by pisarenko on 12.11.2015.
 */
public final class TrelloStatsAppTests {

    private static final String LIST_FILTER = "-listfilter=" + "56752f5f20caff5e755d878f;" +
        "56752f6e40b9b3c312f6e092;" +
        "56752f76aab43239ef386417;" +
        "56782b122a46984282b8d1c1;" +
        "565e92df95395f71e2b9f00f";

    @Test
    public void extractFileNameSunnyDay() {
        final TrelloStatsApp out =
            new TrelloStatsApp(Mockito.mock(ITextStorage.class));
        Assertions.assertThat(out.extractDate("2015-12-11_trelloStats.json"))
                .isEqualTo("2015-12-11");
        Assertions.assertThat(out.extractDate("F:\\trello-stats\\src\\test\\resources\\2015-12-11_trelloStats.json"))
                .isEqualTo("2015-12-11");
        Assertions.assertThat(out.extractDate("src\\test\\resources\\2015-12-11_trelloStats.json"))
                .isEqualTo("2015-12-11");
        Assertions.assertThat(
                out.extractDate(
                        "F:\\2015-12-30\\trello-stats\\src\\test\\resources\\2015-12-11_trelloStats.json"
                )
        ).isEqualTo("2015-12-11");
    }
    @Test
    public void integrationTest() throws IOException {
        final TestStorage textStorage = new TestStorage();
        final TrelloStatsApp out =
            new TrelloStatsApp(textStorage);
        out.run(new String[] {
                "src/test/resources/2015-12-11_trelloStats.json"}
        );
        Assertions.assertThat(textStorage.text()).isEqualTo(
            FileUtils.readFileToString(
                new File("src/test/resources/2015-12-11_expectedResult.txt"),
                "UTF-8"
            )
        );
    }
    @Test
    public void hasFilterListDetectsFilterList() {
        final TestStorage textStorage = new TestStorage();
        final TrelloStatsApp out =
            new TrelloStatsApp(textStorage);
        // Run method under test
        final boolean result = out.hasFilterList(new String[]{
            LIST_FILTER, "someFile.json"});
        // Verify
        Assertions.assertThat(result).isTrue();
    }
    @Test
    public void hasFilterListDetectsAbsenceOfFilterList() {
        final TestStorage textStorage = new TestStorage();
        final TrelloStatsApp out =
            new TrelloStatsApp(textStorage);
        // Run method under test
        final boolean result = out.hasFilterList(new String[]{
            "someFile1.json",
            "someFile2.json",
        });
        // Verify
        Assertions.assertThat(result).isFalse();
    }
    @Test
    public void runCallsSetupFilterListIfThereIsOne() {
        final TestStorage textStorage = new TestStorage();
        final TrelloStatsApp out =
            Mockito.spy(
                new TrelloStatsApp(textStorage)
            );
        final String[] args = new String[] {"file1.json", "file2.json"};
        Mockito.doReturn(true).when(out).hasFilterList(args);
        Mockito.doNothing().when(out).readStatItems(
            Mockito.eq(args),
            Mockito.anyList(),
            Mockito.anyList(),
            Mockito.anyMap()
        );
        Mockito.doReturn(new StringBuilder()).when(out)
            .convertToCsv(Mockito.anyList(),
                Mockito.anyMap(), Mockito.anyList());
        Mockito.doNothing().when(out).setupFilterList(
            Mockito.anyList(),
            Mockito.anyString()
        );
        // Run method under test
        out.run(args);
        // Verify
        Mockito.verify(out).setupFilterList(
            Mockito.anyList(),
            Mockito.eq(args[0])
        );
    }
    @Test
    public void setupFilterListSunnyDay() {
        final TestStorage textStorage = new TestStorage();
        final TrelloStatsApp out =
            new TrelloStatsApp(textStorage);
        final List<String> relevantListIds = new ArrayList<String>(5);
        // Run method under test
        out.setupFilterList(relevantListIds, LIST_FILTER);
        // Verify
        Assertions.assertThat(relevantListIds.size()).isEqualTo(5);
        Assertions.assertThat(relevantListIds.get(0))
            .isEqualTo("56752f5f20caff5e755d878f");
        Assertions.assertThat(relevantListIds.get(1))
            .isEqualTo("56752f6e40b9b3c312f6e092");
        Assertions.assertThat(relevantListIds.get(2))
            .isEqualTo("56752f76aab43239ef386417");
        Assertions.assertThat(relevantListIds.get(3))
            .isEqualTo("56782b122a46984282b8d1c1");
        Assertions.assertThat(relevantListIds.get(4))
            .isEqualTo("565e92df95395f71e2b9f00f");
    }
    @Test
    public void readStatItemsStartsWithSecondFileIfListFilterPresent()
        throws IOException {
        readStatItemsTestLogic(false, 0);
    }

    private void readStatItemsTestLogic(
        final boolean relevantListIdsEmpty,
        final int numberOfFirstFileProcessings) throws IOException {
        final TestStorage textStorage = new TestStorage();
        final TrelloStatsApp out =
            Mockito.spy(
                new TrelloStatsApp(textStorage)
            );
        final String[] args = new String[] {"file1", "file2"};
        final List<String> relevantListIds = Mockito.mock(List.class);
        Mockito.when(relevantListIds.isEmpty())
            .thenReturn(relevantListIdsEmpty);
        final List<TrelloStatisticsItem> statitems = Mockito.mock(List.class);
        final Map<String, TrelloList> listDataById = Mockito.mock(Map.class);
        Mockito.doReturn(null).when(out).processFile(
            Mockito.anyString(),
            Mockito.anyMap(),
            Mockito.anyList()
        );
        // Run method under test
        out.readStatItems(args, relevantListIds, statitems, listDataById);
        // Verify
        Mockito.verify(
            out,
            Mockito.times(numberOfFirstFileProcessings)
        ).processFile(
            "file1",
            listDataById,
            relevantListIds
        );
        Mockito.verify(out).processFile("file2", listDataById, relevantListIds);
    }

    @Test
    public void readStatItemsStartsWithFirstFileIfListFilterPresent()
        throws IOException {
        readStatItemsTestLogic(true, 1);
    }
    @Test
    public void writeHeaderWritesOutRelevantHeadersOnly() {
        final TestStorage textStorage = new TestStorage();
        final TrelloStatsApp out =
            Mockito.spy(
                new TrelloStatsApp(textStorage)
            );
        final List<TrelloList> listsSortedByPos = new ArrayList<TrelloList>(2);
        listsSortedByPos.add(
            createTrelloList(
                "id1",
                "name1",
                Double.valueOf(0.)
            )
        );
        listsSortedByPos.add(
            createTrelloList(
                "id2",
                "name2",
                Double.valueOf(1.)
            )
        );
        final StringBuilder builder = new StringBuilder();
        final List<String> relevantListIds = new ArrayList<String>(1);
        relevantListIds.add("id2");
        // Run method under test
        out.writeHeader(listsSortedByPos, builder, relevantListIds);
        // Verify
        Assertions.assertThat(builder.toString()).isEqualTo(
            "Date;name2;"
                + TrelloStatsApp.NEWLINE
                + "Date;id2;"
                + TrelloStatsApp.NEWLINE
        );
    }

    private TrelloList createTrelloList(
        final String id,
        final String name,
        final Double pos) {
        return new TrelloList(id, name, pos);
    }

    @Test
    public void writeHeaderWritesOutAllHeadersIfRelevantListIdsNotSpecified() {
        final TestStorage textStorage = new TestStorage();
        final TrelloStatsApp out =
            Mockito.spy(
                new TrelloStatsApp(textStorage)
            );
        final List<TrelloList> listsSortedByPos = new ArrayList<TrelloList>(2);
        listsSortedByPos.add(
            createTrelloList(
                "id1",
                "name1",
                Double.valueOf(0.)
            )
        );
        listsSortedByPos.add(
            createTrelloList(
                "id2",
                "name2",
                Double.valueOf(1.)
            )
        );
        final StringBuilder builder = new StringBuilder();
        final List<String> relevantListIds = new ArrayList<String>(0);
        // Run method under test
        out.writeHeader(listsSortedByPos, builder, relevantListIds);
        // Verify
        Assertions.assertThat(builder.toString()).isEqualTo(
            "Date;name1;name2;"
                + TrelloStatsApp.NEWLINE
                + "Date;id1;id2;"
                + TrelloStatsApp.NEWLINE
        );
    }
    @Test
    public void appendListCardCountAppendsDataIfListRelevant() {
        final TestStorage textStorage = new TestStorage();
        final TrelloStatsApp out = new TrelloStatsApp(textStorage);
        final StringBuilder builder = new StringBuilder();
        final TrelloStatisticsItem sitem =
            Mockito.mock(TrelloStatisticsItem.class);
        Mockito.when(sitem.numberOfCards("listId1")).thenReturn(10);
        Mockito.when(sitem.numberOfCards("listId2")).thenReturn(20);

        final TrelloList list1 = Mockito.mock(TrelloList.class);
        Mockito.when(list1.id()).thenReturn("listId1");
        final TrelloList list2 = Mockito.mock(TrelloList.class);
        Mockito.when(list2.id()).thenReturn("listId2");

        final List<String> relevantListIds = new ArrayList<String>(1);
        relevantListIds.add("listId1");
        // Run method under test
        out.appendListCardCount(builder,
            sitem, list1, relevantListIds);
        // Verify
        Assertions.assertThat(builder.toString()).isEqualTo("10;");
    }
    @Test
    public void appendListCardCountDoesNotAppendDataIfListNotRelevant() {
        final TestStorage textStorage = new TestStorage();
        final TrelloStatsApp out = new TrelloStatsApp(textStorage);
        final StringBuilder builder = new StringBuilder();
        final TrelloStatisticsItem sitem =
            Mockito.mock(TrelloStatisticsItem.class);
        Mockito.when(sitem.numberOfCards("listId1")).thenReturn(10);
        Mockito.when(sitem.numberOfCards("listId2")).thenReturn(20);

        final TrelloList list1 = Mockito.mock(TrelloList.class);
        Mockito.when(list1.id()).thenReturn("listId1");
        final TrelloList list2 = Mockito.mock(TrelloList.class);
        Mockito.when(list2.id()).thenReturn("listId2");

        final List<String> relevantListIds = new ArrayList<String>(1);
        relevantListIds.add("listId1");
        // Run method under test
        out.appendListCardCount(builder,
            sitem, list2, relevantListIds);
        // Verify
        Assertions.assertThat(builder.toString()).isEmpty();
    }

    @Test
    public void appendListCardCountAppendsDataIfListFilterNotSpecified() {
        final TestStorage textStorage = new TestStorage();
        final TrelloStatsApp out = new TrelloStatsApp(textStorage);
        final StringBuilder builder = new StringBuilder();
        final TrelloStatisticsItem sitem =
            Mockito.mock(TrelloStatisticsItem.class);
        Mockito.when(sitem.numberOfCards("listId1")).thenReturn(10);
        Mockito.when(sitem.numberOfCards("listId2")).thenReturn(20);

        final TrelloList list1 = Mockito.mock(TrelloList.class);
        Mockito.when(list1.id()).thenReturn("listId1");
        final TrelloList list2 = Mockito.mock(TrelloList.class);
        Mockito.when(list2.id()).thenReturn("listId2");

        final List<String> relevantListIds = new ArrayList<String>(1);
        // Run method under test
        out.appendListCardCount(builder,
            sitem, list2, relevantListIds);
        // Verify
        Assertions.assertThat(builder.toString()).isEqualTo("20;");
    }
}
