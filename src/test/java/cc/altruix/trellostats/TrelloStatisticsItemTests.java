package cc.altruix.trellostats;

import java.util.Arrays;
import java.util.Collections;
import org.fest.assertions.api.Assertions;
import org.junit.Test;

/**
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public final class TrelloStatisticsItemTests {
    private static final String LIST_ID = "listId";
    @Test
    public void saveListDataSavesListDataIfRelevantListIdsIsEmpty() {
        final TrelloStatisticsItem out =
            new TrelloStatisticsItem("2015-12-21");
        // Run method under test
        out.saveListData(LIST_ID, 10, Collections.<String>emptyList());
        // Verify
        Assertions.assertThat(out.numberOfCards(LIST_ID)).isEqualTo(10);
    }
    @Test
    public void saveListDataSavesListDataIfListIsRelevant() {
        final TrelloStatisticsItem out =
            new TrelloStatisticsItem("2015-12-21");
        // Run method under test
        out.saveListData(
            LIST_ID,
            10,
            Arrays.asList(LIST_ID));
        // Verify
        Assertions.assertThat(out.numberOfCards(LIST_ID)).isEqualTo(10);
    }
    @Test
    public void saveListDataDoesNotSaveListDataIfListNotRelevant() {
        final TrelloStatisticsItem out =
            new TrelloStatisticsItem("2015-12-21");
        // Run method under test
        out.saveListData(
            LIST_ID,
            10,
            Arrays.asList("list2"));
        // Verify
        Assertions.assertThat(out.numberOfCards(LIST_ID)).isNull();
    }
}
