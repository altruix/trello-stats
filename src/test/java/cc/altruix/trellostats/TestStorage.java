package cc.altruix.trellostats;

/**
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public class TestStorage implements ITextStorage {
    private String text;
    public void saveReport(final String report) {
        this.text = report;
    }
    public String text() {
        return this.text;
    }
}
