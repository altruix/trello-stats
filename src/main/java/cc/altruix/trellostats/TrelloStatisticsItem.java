package cc.altruix.trellostats;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pisarenko on 12.11.2015.
 */
public class TrelloStatisticsItem {
    private final String date;
    private Map<String,Integer> numberOfCardsByListId =
        new HashMap<String, Integer>();
    public TrelloStatisticsItem(final String date) {
        this.date = date;
    }
    public void saveListData(final String listId, final int cards,
        final List<String> relevantListIds) {
        if (relevantListIds.isEmpty() || relevantListIds.contains(listId)) {
            numberOfCardsByListId.put(listId, cards);
        }
    }
    public String date() {
        return this.date;
    }

    public Integer numberOfCards(final String listId) {
        return numberOfCardsByListId.get(listId);
    }
}
