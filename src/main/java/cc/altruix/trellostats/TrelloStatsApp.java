package cc.altruix.trellostats;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by pisarenko on 12.11.2015.
 */
public class TrelloStatsApp {
    protected static final String NEWLINE = System.getProperty("line.separator");
    private final ITextStorage textStorage;

    public TrelloStatsApp() {
        this(new DefaultTextStorage());
    }
    public TrelloStatsApp(final ITextStorage textStorage) {
        this.textStorage = textStorage;
    }

    public static void main(final String[] args) {
        final TrelloStatsApp app = new TrelloStatsApp();
        app.run(args);
    }

    protected void run(final String[] args) {
        if (args == null) {
            return;
        }
        if (args.length < 1) {
            return;
        }
        final List<String> relevantListIds = new LinkedList<String>();
        if (hasFilterList(args)) {
            setupFilterList(relevantListIds, args[0]);
        }
        final List<TrelloStatisticsItem> statitems =
            new LinkedList<TrelloStatisticsItem>();
        final Map<String, TrelloList> listDataById =
            new HashMap<String, TrelloList>();
        readStatItems(args, relevantListIds, statitems, listDataById);
        StringBuilder csvBuilder = convertToCsv(
            statitems,
            listDataById,
            relevantListIds
        );
        this.textStorage.saveReport(csvBuilder.toString());
    }

    protected void readStatItems(final String[] args,
        final List<String> relevantListIds,
        final List<TrelloStatisticsItem> statitems,
        final Map<String, TrelloList> listDataById) {
        final int start = relevantListIds.isEmpty() ? 0 : 1;
        for (int i= start;
             i < args.length; i++) {
            final String filename = args[i];
            try {
                statitems.add(
                    processFile(
                        filename,
                        listDataById,
                        relevantListIds)
                );
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    protected void setupFilterList(final List<String> relevantListIds,
        final String listFilterText) {
        final String textWithoutPrefix = listFilterText
            .substring("-listfilter=".length());
        final String[] ids = textWithoutPrefix.split(";");
        for (final String id : ids) {
            relevantListIds.add(id);
        }
    }
    protected boolean hasFilterList(final String[] args) {
        return args[0].startsWith("-listfilter=");
    }

    protected StringBuilder convertToCsv(
        final List<TrelloStatisticsItem> statitems,
        final Map<String, TrelloList> listDataById,
        final List<String> relevantListIds) {
        final List<TrelloList> listsSortedByPos =
            new ArrayList<TrelloList>(listDataById.values().size());
        listsSortedByPos.addAll(listDataById.values());
        Collections.sort(listsSortedByPos, new Comparator<TrelloList>() {
            public int compare(final TrelloList o1, final TrelloList o2) {
                final double pos1 = o1.pos();
                final double pos2 = o2.pos();

                return (int) (pos1 - pos2);
            }
        });
        final StringBuilder builder = new StringBuilder();

        writeHeader(listsSortedByPos, builder, relevantListIds);
        Collections.sort(statitems, new Comparator<TrelloStatisticsItem>() {
            public int compare(final TrelloStatisticsItem o1,
                final TrelloStatisticsItem o2) {
                final String date1 = o1.date();
                final String date2 = o1.date();
                return date1.compareTo(date2);
            }
        });

        for (final TrelloStatisticsItem sitem : statitems) {
            builder.append(sitem.date());
            builder.append(";");
            for (final TrelloList list : listsSortedByPos) {
                appendListCardCount(
                    builder,
                    sitem,
                    list,
                    relevantListIds
                );
            }
            builder.append(NEWLINE);
        }
        return builder;
    }

    protected void appendListCardCount(
        final StringBuilder builder,
        final TrelloStatisticsItem sitem,
        final TrelloList list,
        final List<String> relevantListIds) {
        if (relevantListIds.isEmpty() || relevantListIds.contains(list.id()))
        {
            final Integer cardCount = sitem.numberOfCards(list.id());
            if (cardCount != null) {
                builder.append(cardCount);
            } else {
                builder.append("-1");
            }
            builder.append(";");
        }
    }

    protected void writeHeader(final List<TrelloList> listsSortedByPos,
        final StringBuilder builder,
        final List<String> relevantListIds) {
        builder.append("Date");
        builder.append(";");
        for (final TrelloList list : listsSortedByPos) {
            if (relevantListIds.isEmpty() ||
                relevantListIds.contains(list.id())) {
                builder.append(list.name());
                builder.append(";");
            }
        }
        builder.append(NEWLINE);
        builder.append("Date");
        builder.append(";");
        for (final TrelloList list : listsSortedByPos) {
            if (relevantListIds.isEmpty() ||
                relevantListIds.contains(list.id())) {
                builder.append(list.id());
                builder.append(";");
            }
        }
        builder.append(NEWLINE);
    }

    protected TrelloStatisticsItem processFile(final String filename,
        final Map<String, TrelloList> listDataById,
        final List<String> relevantListIds) throws IOException {
        resetCardCount(listDataById);
        final Map<String, Object> jsonData =
            readJsonData(filename);
        addMissingLists(listDataById, jsonData);
        processCards(listDataById, jsonData);
        final String date = extractDate(filename);
        final TrelloStatisticsItem statItem =
            new TrelloStatisticsItem(date);
        for (final TrelloList list : listDataById.values()) {
            statItem.saveListData(list.id(), list.numberOfCards(),
                relevantListIds);
        }
        return statItem;
    }

    private void processCards(final Map<String, TrelloList> listDataById, final Map<String, Object> jsonData) {
        final List<Map<String, Object>> cards =
            (List<Map<String, Object>>) jsonData.get("cards");
        for (final Map<String,Object> card : cards) {
            if (Boolean.TRUE.equals(card.get("closed"))) {
                continue;
            }
            final TrelloList list = listDataById.get(card.get("idList"));
            if (list != null) {
                list.incrementNumberOfCards();
            }
        }
    }

    private void addMissingLists(final Map<String, TrelloList> listDataById, final Map<String, Object> jsonData) {
        final List<Map<String,Object>> lists =
            (List<Map<String, Object>>) jsonData.get("lists");
        for (final Map<String,Object> listData : lists) {
            if (Boolean.TRUE.equals(listData.get("closed"))) {
                continue;
            }
            final String id = (String) listData.get("id");
            if (!listDataById.containsKey(id)) {
                listDataById.put(id,
                    new TrelloList(
                        id,
                        (String) listData.get("name"),
                        convertToDoubleIfNecessary(listData.get("pos"))
                    )
                );
            }
        }
    }

    private Double convertToDoubleIfNecessary(final Object pos) {
        if (pos instanceof Double) {
            return (Double) pos;
        } else if (pos instanceof Integer) {
            return Double.valueOf(((Integer)pos).doubleValue());
        }
        return null;
    }

    private void resetCardCount(final Map<String, TrelloList> listDataById) {
        for (final TrelloList list : listDataById.values()) {
            list.reset();
        }
    }

    protected Map<String, Object> readJsonData(final String filename)
        throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        final Map<String,Object> jsonData =
            mapper.readValue(new File(filename), Map.class);

        return jsonData;
    }

    protected String extractDate(final String filename) {
        final Pattern pattern =
            Pattern.compile(".*(\\d\\d\\d\\d-\\d\\d-\\d\\d).*");
        final Matcher matcher = pattern.matcher(filename);
        if (matcher.matches()) {
            return matcher.group(1);
        }
        return "";
    }
}
