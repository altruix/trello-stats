package cc.altruix.trellostats;

/**
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public class TrelloList {
    private final String id;
    private final String name;
    private final Double pos;
    private int numberOfCards = 0;

    public TrelloList(final String id, final String name, final Double pos) {
        this.id = id;
        this.name = name;
        this.pos = pos;
    }
    public void incrementNumberOfCards() {
        this.numberOfCards++;
    }
    public int numberOfCards() {
        return this.numberOfCards;
    }
    public Double pos() {
        return this.pos;
    }

    public void reset() {
        this.numberOfCards = 0;
    }
    public String id() {
        return this.id;
    }
    public String name() {
        return this.name;
    }
}
