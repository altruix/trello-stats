package cc.altruix.trellostats;

/**
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public interface ITextStorage {
    void saveReport(final String report);
}
