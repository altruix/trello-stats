package cc.altruix.trellostats;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import org.apache.commons.io.FileUtils;

/**
 * @author Dmitri Pisarenko (dp@altruix.co)
 * @version $Id$
 * @since 1.0
 */
public class DefaultTextStorage implements ITextStorage {
    public void saveReport(final String report) {
        System.out.println(report);

        try {
            FileUtils.writeStringToFile(
                new File(composeFileName()),
                report, "UTF-8");
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
    private String composeFileName() {
        return String.format("trello-stats-%d.csv", new Date().getTime());
    }
}
